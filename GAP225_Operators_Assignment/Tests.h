#pragma once

#include <cstdlib>
#include <iostream>
#include <ostream>

void Test(const char* pTestName, bool test)
{
	std::cout << pTestName << ": " << (test ? "SUCCESS" : "FAILURE") << std::endl;
}

class Vector3
{
private:
	float m_x;
	float m_y;
	float m_z;
public:
	Vector3(float x, float y, float z)
		: m_x(x)
		, m_y(y)
		, m_z(z)
	{ }
	Vector3()
		: Vector3(0, 0, 0)
	{ }
	float GetX() const { return this->m_x; }
	float GetY() const { return this->m_y; }
	float GetZ() const { return this->m_z; }

	static Vector3 GenerateUnitVector() { return Vector3(1, 1, 1); }
	void operator+= (const Vector3& other)
	{
		this->m_x += other.GetX();
		this->m_y += other.GetY();
		this->m_z += other.GetZ();
	}
	void operator-= (const Vector3& other)
	{
		this->m_x -= other.GetX();
		this->m_y -= other.GetY();
		this->m_z -= other.GetZ();
	}
	void operator*= (const float other)
	{
		this->m_x *= other;
		this->m_y *= other;
		this->m_z *= other;
	}
	void operator/= (const float other)
	{
		this->m_x /= other;
		this->m_y /= other;
		this->m_z /= other;
	}

	float GetLength()
	{
		return sqrt(GetSquareLength());
	}

	// See https://www.youtube.com/watch?v=nl24ps-IZIU&ab_channel=EngiGames.
	float GetSquareLength()
	{
		return pow(GetX(), 2) + pow(GetY(), 2) + pow(GetZ(), 2);
	}

	float GetDotProduct(const Vector3& right) const
	{
		return GetX() * right.GetX() + GetY() * right.GetY() + GetZ() * right.GetZ();
	}

	Vector3 GetCrossProduct(const Vector3& right) const
	{
		return Vector3(GetY() * right.GetZ() - GetZ() * right.GetY(),
			GetZ() * right.GetX() - GetX() * right.GetZ(),
			GetX() * right.GetY() - GetY() * right.GetX());
	}
};

bool operator==(const Vector3& self, const Vector3& other)
{
	return self.GetX()  == other.GetX() && self.GetY() == other.GetY() && self.GetZ() == other.GetZ();
}

Vector3 operator- (const Vector3& other)
{
	return Vector3(-other.GetX(), -other.GetY(), -other.GetZ());
}

Vector3 operator+ (const Vector3& self, const Vector3& other)
{
	return Vector3(self.GetX() + other.GetX(), 
		self.GetY() + other.GetY(), 
		self.GetZ() + other.GetZ());
}

Vector3 operator- (const Vector3& self, const Vector3& other)
{
	return Vector3(self.GetX() - other.GetX(),
		self.GetY() - other.GetY(),
		self.GetZ() - other.GetZ());
}

Vector3 operator* (const Vector3& self, const float other)
{
	return Vector3(self.GetX() * other,
		self.GetY() * other,
		self.GetZ() * other);
}

Vector3 operator* (const float other, const Vector3& self)
{
	return Vector3(other * self.GetX(),
		other * self.GetY(),
		other * self.GetZ());
}

Vector3 operator/ (const Vector3& self, const float other)
{
	return Vector3(self.GetX() / other,
		self.GetY() / other,
		self.GetZ() / other);
}

std::ostream& operator<< (std::ostream& os, const Vector3& other)
{
	os << "Vector3{" << other.GetX() << "," << other.GetY() << "," << other.GetZ() << "}";
	return os;
}

void RunAllTests()
{
		//TIP: Use conditional compilation to remove tests that you haven't solved yet.

	const Vector3 kZeroVector{};
	Test("Default ctor: assigns 0 to x", kZeroVector.GetX() == 0);
	Test("Default ctor: assigns 0 to y", kZeroVector.GetY() == 0);
	Test("Default ctor: assigns 0 to z", kZeroVector.GetZ() == 0);

	const Vector3 kConstTestVector{ 1, 2, 3 };
	Test("Parameterized ctor: first parameter assigns to x", kConstTestVector.GetX() == 1);
	Test("Parameterized ctor: first parameter assigns to x", kConstTestVector.GetY() == 2);
	Test("Parameterized ctor: first parameter assigns to x", kConstTestVector.GetZ() == 3);

	const Vector3 kUnitVector = Vector3::GenerateUnitVector();
	Test("GenerateUnitVector returns Vector{1,1,1}", kUnitVector.GetX() == kUnitVector.GetY() == kUnitVector.GetZ() == 1);
	Test("Comparison operator implemented", kUnitVector == kUnitVector);

	const Vector3 kTestVector1 = -kUnitVector;
	Test("Inversion operator implemented", kTestVector1 == Vector3{ -1,-1,-1 });

	Vector3 testVector2 = kUnitVector + kUnitVector;
	Test("Addition operator implemented", testVector2 == Vector3{ 2,2,2 });

	testVector2 += kUnitVector;
	Test("Addition assignment operator implemented", testVector2 == Vector3{ 3,3,3 });

	Vector3 testVector3 = kUnitVector - kUnitVector;
	Test("Subtraction operator implemented", testVector3 == kZeroVector);

	testVector3 -= kUnitVector;
	Test("Subtraction assignment operator implemented", testVector3 == Vector3{ -1,-1, -1 });

	Vector3 testVector4 = kUnitVector * 2;
	Test("Scalar multiplication operator implemented", testVector4 == Vector3{ 2,2,2 });

	Vector3 testVector5 = 2 * kUnitVector;
	Test("Scalar multiplication operator is transitive (the float can be first)", testVector5 == testVector4);

	testVector5 *= 2;
	Test("Scalar multiplication assignment operator implemented", testVector5 == Vector3{ 4,4,4 });

	Vector3 testVector6 = kUnitVector / 2;
	Test("Scalar division operator implemented", testVector6 == Vector3{ 0.5f, 0.5f, 0.5f });

	testVector6 /= 2;
	Test("Scalar division assignment operator implemented", testVector6 == Vector3{ 0.25f, 0.25f, 0.25f });


	// This won't be testable in a true unit test because it doesn't return a comparable value.
	// It succeeds if it prints this: Vector3{1,2,3}
	// (It should not include a newline character or std::endl.)
	std::cout << kConstTestVector << std::endl;

#if 0	// Move this below each test as you solve it, or set it to 1 to test everything.
#endif
}